# growth_projection

Quick projection of growth for a specific number of months

## Usage
    ./normal_growth.py 
    -c 12           #Cycles per projection - imagine monthly growth, 12 months
    -p 1000000      #Number of projection runs
    -s 1            #Starting population size
    -g 0.005         #Growth rate per cycle (in this case, montly)
    -s 0.004         #Std. deviation of growth rate

## Output
    Results:  
     Start: 1.0  
     Mean End: 1.0616748577270603  
     Std. Dev: 0.014644195464854414
